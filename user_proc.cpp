/* Emily Beauto
 * CS 4760
 * Project 6 Memory Management
 * 5/9/23
 * user_proc.cpp
 */


#include<unistd.h>
#include<sys/types.h>
#include<stdio.h>
#include<stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <errno.h>
#include <string.h>
#include <ctime>

#define PERMS 0644
typedef struct msgbuffer {
	long mtype;
	int choice;
	int address;
	int pid;
} msgbuffer;



int main(int argc, char** argv) 
{
	//allocation; establish clock connection	
  	const int keySec = 1234; 
        const int keyNano = 1235; 
        
	int idSec = shmget( keySec , sizeof(int)* 2 , IPC_CREAT | 0666  );
        if (idSec <= 0) {
                fprintf(stderr,"Shared memory get failed\n");
                exit(1);
        }

        int idNano = shmget( keyNano , sizeof(int)* 2 , IPC_CREAT | 0666  );
        if (idNano <= 0) {
                fprintf(stderr,"Shared memory get failed\n");
                exit(1);
        }

        //clock attaching- get pointer to shared block
        int *clockSec = (int*)shmat( idSec, 0 , 0 );
        if (clockSec <= 0) {
                fprintf(stderr,"Shared memory attach failed\n");
                exit(1);
        }

         //clock attaching- get pointer to shared block
        int *clockNano = (int*)shmat( idNano, 0, 0 );
        if (clockNano <= 0) {
                fprintf(stderr,"Shared memory attach failed\n");
                exit(1);
        }


	//establish message queue
	msgbuffer buf;
	buf.mtype = 1;
	int msqid = 0;
	key_t key;

	// get a key for our message queue
	if ((key = ftok("msgq.txt", 1)) == -1) {
		perror("ftok");
		exit(1);
	}

	// create our message queue
	if ((msqid = msgget(key, PERMS)) == -1) {
		perror("msgget in child");
		exit(1);
	}



	srand(time(0) ^ getpid());		
	bool termFlag = false;
	int addr = -1;	//address
	int r = -1;	//percentage
	int requests= 0;	//requests made
	int result = 0;
        result = (rand()% (1100 -900)) + 900;

	while(true)
	{
		addr = (rand() % 32) * 1024 + (rand() %  1024);	//address
		r = rand() % 100; 		//0-99
		if(requests >= result)
                {
			termFlag = true;
                }	

		if(r <= 80)	//80% reading
		{
			requests++;
			buf.choice = 1;
			buf.mtype = getppid();
                        buf.address = addr;
                        buf.pid = getpid();
                        //send msg to oss with R number
                        if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                perror("Read msgsnd to parent failed\n");
    	                    	exit(1);
                        }
                        // receive a message, but only one for us
                        if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                        	perror("Read failed to receive message from parent\n");
                                exit(1);
                        }
		}
		else if(r >= 81 && r <=94)	//writing 
		{
			requests++;
			buf.choice = 2;
			buf.mtype = getppid();
                        buf.address = addr;
                        buf.pid = getpid();
                        //send msg to oss with R number
                        if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                perror("Writing msgsnd to parent failed\n");
                                exit(1);
                        }
                        // receive a message, but only one for us
                        if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                perror("Writing failed to receive message from parent\n");
                                exit(1);
                        }
		}
		else if(r >= 94 && r <= 99 && termFlag== true) //termination
		{
			buf.choice = 3;
			buf.mtype = getppid();
			buf.address = addr;
                        buf.pid = getpid();
			//send msg to oss with R number
                       	if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
        	        	perror("Term msgsnd to parent failed\n");
                                exit(1);
                       	}
			if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                       		perror("Term failed to receive message from parent\n");
                      	        exit(1);
                     	}
			exit(0);
		}
	}
	return 0;
}

