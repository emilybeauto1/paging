Emily Beauto
Project 6 Memory Management


How to compile and run:
	-Type make 
	-Then execute to following command:

	
Calling:
	./oss -f logfile [-v] 
		-logfile is replaced with the name of file to open for output.
		-optional v to turn off verbose 
		    If -f is not provided automatic "logfile" will be opened
		    If -v is not included verbose is automoatically turned on 
		EX: ./oss -f file -v   	   or        ./oss -h    for help message

	
	Parameters for user_proc.cpp
	./user_proc
		where communication is sent via message queue
		EX: ./user_proc    	//executed in oss.cpp

*** SERVER LOAD WILL CHANGE NUMBER OF PROCESSESS RUNNING, LAUNCH RANGE WHEN TESTING WAS 18-100 ***

*** Logfile prints as many lines as needed as no limit was specified in the directions on Canvas ***


Communicating:
	-used message queues to communicate requests and synchronization


Policy to find victim frame:
	-Victim frame to replace is based on FIFO replacement therory meaning that the head of the headq is the frame which will be swapped.


Logging Files:
	-git (.git subdirectory) was used to keep track of versioning files


Outstanding Problems: 
	-N/A


Problems Encounter:
	-Visual Bug when printing frame table. Solved by using seperate print statements
	-Understanding the statistics. Solved by talking with Mark
	-closing message queue. Solved with msgctl.
	-Originally setting up the clock in shared memory, with different keys. Problem was solved after looking at the tutorial and asking questions. 
	-Freeing up memory when signal handling. Solved with global pointers so I can free up memory before terminating program.
	 
