/* Emily Beauto
 * CS 4760
 * Project 6 Memory Management
 * 5/9/23
 * oss.cpp
 */

#include<unistd.h>
#include<sys/types.h>
#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <sys/msg.h>
#include <errno.h>
#include <ctime>
#include <sys/wait.h>
#include <string.h>
#include <queue>
#include <string>  
#include <iostream>
#include <math.h>

using namespace std;

//stackoverflow example of converting int to char * in c
char *convert(char *destination, int i){
	sprintf(destination, "%d", i);
	return destination;
}

#define ITOCHAR(n) convert((char[41]) { 0 },(n))


//for message queue
#define PERMS 0644

typedef struct msgbuffer {
	long mtype;
	int choice;
	int address;
	int pid;
} msgbuffer;

//global variables- frees memory in main and signal handlings 
int keySec, keyNano, idSec, idNano, msqid;
int *clockSec,*clockNano;        


struct pages {
	int occupied;
	int frame;
};


struct frame {
	int occupied;
	int dirty;
	char head;
};

//process table structure
struct PCB {
	int occupied; 		// either true or false
	pid_t pid; 		// process id of this child
	int startSeconds; 	// time when it was forked
	int startNano; 		// time when it was forked
	pages page[32];
};


//frees up memory and kills program (after 60 second timer goes off in main)
static void alarmHandler(int sig)
{
	shmdt( clockSec );    // Detach from the shared memory segment
	shmctl( idSec, IPC_RMID, NULL ); // Free shared memory segment shm_id
	shmdt( clockNano );    // Detach from the shared memory segment
	shmctl( idNano, IPC_RMID, NULL ); // Free shared memory segment shm_id
        if (msgctl(msqid, IPC_RMID, NULL) == -1) {
                perror("msgctl to get rid of queue in parent failed");
                exit(1);
        }

	printf("60 seconds are up! oss.c is now:\n");
	killpg(getpid(), SIGKILL);
	exit(EXIT_SUCCESS);
}


//catches CTRL ^ C, frees up memory and terminates program
static void interruptSig(int sig)
{
	shmdt( clockSec );    // Detach from the shared memory segment
        shmctl( idSec, IPC_RMID, NULL ); // Free shared memory segment shm_id
        shmdt( clockNano );    // Detach from the shared memory segment
        shmctl( idNano, IPC_RMID, NULL ); // Free shared memory segment shm_id
 	if (msgctl(msqid, IPC_RMID, NULL) == -1) {
                perror("msgctl to get rid of queue in parent failed");
                exit(1);
        }

	printf("\nInterrupt Signal CTRL C caught\n");
	exit(EXIT_SUCCESS);
}


//increments clock
void incrementClock(int *sec, int *nano, int timePassed)
{
        *nano += timePassed;
        if(*nano >= 1000000000)
        {
                *nano -= 1000000000;
                (*sec)++;
        }
}



//adds the process' pid, starting sec, starting nanoseconds and resources to a blank spot in the process table
void addProcess(struct PCB pTable[], int pid, int sec, int nano)
{
	int i;
	for(i = 0; i < 18; i++)
	{
		if(pTable[i].occupied == 0) //if not occupied
		{
			pTable[i].occupied = 1;
			pTable[i].pid = pid;
			pTable[i].startSeconds = sec;
			pTable[i].startNano = nano;	
			return;
		}

	}	
}


//remove process from the process table by searching for the pid
void removeProcess(struct PCB pTable[], int pid)
{
	int i;
	for(i = 0; i < 18; i++)
	{
		if(pTable[i].pid == pid)
		{
			pTable[i].occupied = 0;
                        pTable[i].pid = 0;
                        pTable[i].startSeconds = 0;
                        pTable[i].startNano = 0;
                        return;
		}
	}

}

//prints process table to oss and to logfile
void printTable(struct PCB pTable[], int sec, int nano, FILE * logfile)
{
//	printf("OSS PID: %d SysClock: %d SysclockNano: %d\n", getpid(), sec, nano);
//        printf("Process Table:\n");
//        printf("Entry   Occupied\tPID\tPages\tStartS\tStartN\n");
	fprintf(logfile, "OSS PID: %d SysClock: %d SysclockNano: %d\n", getpid(), sec, nano);
        fprintf(logfile, "Process Table:\n");
        fprintf(logfile, "Entry   Occupied\tPID\tPages\tStartS\tStartN\n");
	for(int i =0; i < 18; i++)
        {
		int counter = 0;
		for(int j = 0; j< 32; j++)
		{
			if(pTable[i].page[j].occupied == 1)
			{
				counter++;
			}
        	}
//		printf("P%d\t%d\t\t%d\t%d\t%d\t%d\n", i, pTable[i].occupied, pTable[i].pid, counter, pTable[i].startSeconds, pTable[i].startNano);
		fprintf(logfile, "P%d\t%d\t\t%d\t%d\t%d\t%d\n", i, pTable[i].occupied, pTable[i].pid, counter, pTable[i].startSeconds, pTable[i].startNano);
	}
        return;
}

//prints frame table to oss and logfile
void printFrame(struct frame fTable[], FILE * logfile, queue<int> headq )
{
//	printf("\t\tOccupied\tDirty\tHeadOfFIFO\n");
        fprintf(logfile, "\t\tOccupied\tDirty\tHeadOfFIFO\n");
	for(int i = 0; i< 256; i++)
	{
		if(i == headq.front())
		{
//			 printf("Frame %d:\t%d\t\t%d\t*\n", i, fTable[i].occupied, fTable[i].dirty);
       		         fprintf(logfile, "Frame %d:\t%d\t\t%d\t*\n", i, fTable[i].occupied, fTable[i].dirty);
       
		}
		else{

//			printf("Frame %d:\t%d\t\t%d\n", i, fTable[i].occupied, fTable[i].dirty);
                	fprintf(logfile, "Frame %d:\t%d\t\t%d\n", i, fTable[i].occupied, fTable[i].dirty);
		}
	}
	return;
}





//print queues used to visually see queues work
void showq(queue<int> gq, FILE * logfile)
{
    queue<int> g = gq;
    while (!g.empty()) {
//        printf("%d ", g.front());
        fprintf(logfile, "%d ", g.front());
        g.pop();
    }
//	printf("\n");
	fprintf(logfile, "\n");
}




//heart of the oss.cpp program
int main(int argc, char** argv) 
{
	//set up signal catches
	signal(SIGALRM, alarmHandler);
	signal(SIGINT, interruptSig);
	alarm(60);		//timer for termination after 60 real life seconds
	
	//set up and initalize processTable
	struct PCB processTable[18];
	for(int j = 0; j < 18; j++)
	{
		processTable[j].occupied = 0;
		processTable[j].pid = 0;
		processTable[j].startSeconds = 0;
		processTable[j].startNano = 0;
		for(int i = 0; i< 32; i++)
		{
			processTable[j].page[i].occupied = 0;
			processTable[j].page[i].frame = -1;
		}	
	}

	struct frame frameTable[256];
	for(int i = 0; i < 256; i++)
	{
		frameTable[i].occupied = 0;
		frameTable[i].dirty = 0;
		frameTable[i].head= ' ';
	}

	//allocation:
        keySec = 1234;
        keyNano = 1235;

        idSec = shmget( keySec , sizeof(int)* 2 , IPC_CREAT | 0666  );
        if (idSec <= 0) {
                fprintf(stderr,"Shared memory get failed\n");
                exit(1);
        }

        idNano = shmget( keyNano , sizeof(int)* 2 , IPC_CREAT | 0666  );
        if (idNano <= 0) {
                fprintf(stderr,"Shared memory get failed\n");
                exit(1);
        }

        //clock attaching- get pointer to shared block     
	clockSec = (int*)shmat( idSec, 0 , 0 );
        if (clockSec <= 0) {
                fprintf(stderr,"Shared memory attach failed\n");
                exit(1);
        }

        //clock attaching- get pointer to shared block
        clockNano = (int*)shmat( idNano, 0, 0 );
        if (clockNano <= 0) {
                fprintf(stderr,"Shared memory attach failed\n");
                exit(1);
        }

	//intializing clock with offset of 1 sec
        *clockSec = 1;
        *clockNano = 0;


	//handles the getopt options, and sets the options equal to correct values
	int option;
	int fileFlag = 0;
	int verbose = 1;
	char* fileName;
	FILE * logfile;	

	//only have options -h for help and -f for logfile name
	while((option = getopt(argc, argv, "hf:v")) != -1 )
	{
		switch(option)
		{
			case 'h':
			{
				printf("To run program type: \n./oss -f logfile -v\n\n");
				printf("Where logfile is the optional name of a file and v is use to turn off verbose\n");
				return (EXIT_SUCCESS);
			}
			case 'f':
			{
				fileFlag = 1;
				fileName = optarg;
				break;
			}
			case 'v':
			{
				verbose = 0;
				break;
			}
			default:
			{
				printf("Error. Case not found.\n");
				exit(EXIT_SUCCESS);
			}

		}

	}

	//creates logfile	
	if(fileFlag == 1)	//-f filename given
	{
		if((logfile = fopen(fileName, "w"))==NULL)
		{
			printf("oss: Failed to open file. Sending kill singal.\n");
			kill(getppid(), SIGTERM);
			exit(EXIT_FAILURE);
		}
	}
	else{	//no -f filename given, automatic name "logfile"
		if((logfile = fopen("logfile", "w"))==NULL)
                {
                        printf("oss: Failed to open file. Sending kill singal.\n");
                        kill(getppid(), SIGTERM);
                        exit(EXIT_FAILURE);
                }
	}



	//set up message queue
	msgbuffer buf0;
	key_t key;
	system("touch msgq.txt");

	// get a key for our message queue
	if ((key = ftok("msgq.txt", 1)) == -1) {
		perror("ftok");
		exit(1);
	}

	// create our message queue
	if ((msqid = msgget(key, PERMS | IPC_CREAT)) == -1) {
		perror("msgget in parent");
		exit(1);
	}

	printf("Message queue set up. Please wait while program runs...\n");


        msgbuffer rcvbuf;
	//creates blocked resource queues
	queue<int> headq;
	int maxTimeBetweenNewProcsNS = 500000000;
	int maxTimeBetweenNewProcsSecs = 0;
	int proc = 100;			//max processes
	int children = 0;
	int terminateChild = 0;
	int choice = 0;		//msg for whats happening
	int decision =0;	//msg .choice 
	int process = 0;	//msg .pid
	int addr = -1;		//msg .address
	bool flagoccupied = false;	//check space in processTable
	int nextLaunchSec= 0, nextLaunchNano= 0;
	int sec = *clockSec; 	//for deadlock detection	
	int timePassed = 0;	

	//stats
	int pagefault=0;
	int totalMsgs=0;
	long int delayTime = 0;
	long int delaySec= 0;
	int totalMemAccess= 0;
	int readWrite= 0;
	srand(time(NULL));
	maxTimeBetweenNewProcsNS = rand()% (maxTimeBetweenNewProcsNS + 1);	
	maxTimeBetweenNewProcsSecs = rand()% (maxTimeBetweenNewProcsSecs + 1);
	printf("max time between %d:%d\n", maxTimeBetweenNewProcsSecs, maxTimeBetweenNewProcsNS);
	//five second timer
	time_t stime = time(0);
    	long int ctime = stime;
    	long int ntime = stime+2;
	bool twoSecFlag = false;
       

	while(terminateChild < proc)		//loop till max processes
	{
		if(*clockSec > sec)
        	{
                	sec= *clockSec + 1;     //every second print frames
			fprintf(logfile, "Current memory layout at time %d:%d is:\n", *clockSec, *clockNano);
			printFrame(frameTable, logfile, headq);
			
		}
		//check if two seconds have passed, if so flag, else update time
		if(ctime >= ntime && twoSecFlag == false)
		{
			twoSecFlag =true;	
			proc = children;
		}
		else if(ctime < ntime)
		{
			ctime =time(0);
		}
		
		//check if time to launch new process
		if((*clockSec > nextLaunchSec ||  *clockSec == nextLaunchSec && *clockNano >= nextLaunchNano) && twoSecFlag == false)
		{
			nextLaunchSec = maxTimeBetweenNewProcsSecs + *clockSec;
       		        nextLaunchNano = maxTimeBetweenNewProcsNS + *clockNano;
                	if (nextLaunchNano > 1000000000)
                	{
                        	nextLaunchNano -= 1000000000;
                        	nextLaunchSec++;
                	}
			//check if room in process table
			flagoccupied = false;
			for (int i = 0; i < 18; i++)
			{
				if(processTable[i].occupied == 0)
				{	
					flagoccupied = true;
					break;
				}
			}	
			//if room in process table and children forked less than max processes, fork new child
			if(children < proc && flagoccupied == true)
			{
				// lets fork off a child
                		children++; 
				pid_t pid = fork();
				if (pid > 0) 
				{
                        		//add to process table and initalize resources
                        		addProcess(processTable, pid, *clockSec, *clockNano);
					//printf("Generating process with PID %d at time %d:%d\n", pid, *clockSec, *clockNano);
                                        fprintf(logfile, "Generating process with PID %d at time %d:%d\n", pid, *clockSec, *clockNano);
					//uncomment for debugging
                                	printTable(processTable, *clockSec, *clockNano, logfile);
       				}	
                		else if (pid == 0)  
				{
					//send message to exceute
				        // in child, so lets exec off child executable
	        		        execlp("./user_proc",(char *)NULL);
                        		// should never get here if exec worked
	               			printf("Exec failed for first child\n");
	               			exit(1);
               			}
               			else {      // fork error
         	        	       	perror("fork failed in parent");
                		}
			}
		}

	


	
		rcvbuf.pid = -10;		
		// See if message is in msg queue
                if (msgrcv(msqid, &rcvbuf,sizeof(msgbuffer), getpid(), IPC_NOWAIT) == -1) {
               	//printf("No message in Oss\n");
                }

		//stops the termination of already terminated pids		
		if(rcvbuf.pid != -10)
		{	


		decision = rcvbuf.choice;	//whatss happening read, write, terminate;
		addr = rcvbuf.address;		//address
		process = rcvbuf.pid;		//child process pid for lookup in table
		int page = (int)floor(addr/1024);	
		totalMsgs++;
		//printf("page %d\n", page);
		if(decision == 1)	//read
		{
			readWrite++;
//			printf("Master: PID %d requesting read of address %d in page %d at time %d:%d\n", process, addr, page, *clockSec, *clockNano);
      	             	fprintf(logfile, "Master: PID %d requesting read of address %d in page %d at time %d:%d\n", process, addr, page, *clockSec, *clockNano);
                	for(int i = 0; i<18; i++)
                        {
                                if(processTable[i].pid == process)      //get correct process
                                {
                                        if(processTable[i].page[page].occupied == 1)    //if page occupied/loaded use it
                                        {
                                                timePassed = 100;
                                                incrementClock(clockSec, clockNano, timePassed);
						delayTime += 100;
                                                if(delayTime >= 1000000000)
                                                {
                                                        delayTime -= 1000000000;
                                                        delaySec++;
                                                }
//						printf("Master: Address %d in frame %d, giving data to PID %d at time %d:%d\n", addr, processTable[i].page[page].frame, process, *clockSec, *clockNano);
						fprintf(logfile, "Master: Address %d in frame %d, giving data to PID %d at time %d:%d\n", addr, processTable[i].page[page].frame, process, *clockSec, *clockNano);
                                                //msg to acknowledge worker
						buf0.mtype = process;
                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                        perror("msgsnd to worker failed\n");
                                                        exit(1);
                                                }
                                        }
                                        else    //page fault	//not loaded in make it loaded
                                        {
                                                bool full = true;
//                                              printf("Master: Address %d is not in a frame, pagefault\n", addr);
                                                fprintf(logfile, "Master: Address %d is not in a frame, pagefault\n", addr);
                                        	pagefault++;
                                                //put in open frame
                                                for(int j =0; j<256; j++)
                                                {
                                                        if(frameTable[j].occupied == 0) //if frame available use it
                                                        {
                                                                full= false;
                                                                frameTable[j].occupied = 1;     //make it occupied
                                                                //save the frame and that it is now occupied
                                                                processTable[i].page[page].occupied = 1;
                                                                processTable[i].page[page].frame = j;
                                                                headq.push(j);
								if(headq.size() <= 1)
								{
									frameTable[headq.front()].head = '*';
                                                                }
//								printf("Master: Open frame found, Loading page %d in frame %d\n", page, processTable[i].page[page].frame);
                                                                fprintf(logfile, "Master: Open frame found, Loading page %d in frame %d\n", page, processTable[i].page[page].frame);
                                                                //acknowledge in worker
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                break;
                                                        }
                                                }
                                                if(full ==true) //no open frame available, swapping
                                                {
//                                                      printf("frame table is full, %d is head\n", headq.front());
                                                        fprintf(logfile, "Frame table is full, %d is head\n", headq.front());
//                                                      printf("Clearing frame %d and swapping PID %d page %d\n", headq.front(), process, page);
                                                        fprintf(logfile, "Clearing frame %d and swapping PID %d page %d\n", headq.front(), process, page);
                                                        for(int j=0; j < 256; j++)
							{
								if(headq.front() == j) //if frame is the head and front of the queue
								{
									if(frameTable[j].dirty == 1)	//if its dirty more time and reset for new frame
									{
//										printf("Master: Dirty bit of frame %d found, adding additional time to the clock\n", j);
                                                                                fprintf(logfile, "Master: Dirty bit of frame %d found, adding additional time to the clock\n", j);
                                                                                incrementClock(clockSec, clockNano, 300000000);
                                                                                frameTable[j].dirty = 0;    //reset dirty bit
	
									}
									//update frame
									frameTable[headq.front()].head = ' ';
							 		headq.push(headq.front());
									headq.pop();
//									fprintf(logfile, "head popped, new head is %d\n", headq.front());
									frameTable[headq.front()].head = '*';
									processTable[i].page[page].frame = j;
									buf0.mtype = process;
                                                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                                perror("msgsnd to worker failed\n");
                                                                                exit(1);
                                                                        }
									break;
								}
							}
                                                }
                                                incrementClock(clockSec, clockNano, 140000000);
						delayTime += 140000000; 			
						if(delayTime >= 1000000000)
						{
							delayTime -= 1000000000;
							delaySec++;
						}
                                        }

                                }
                        }
		}

		else if( decision == 2)	//write
                {
                	readWrite++;
//		        printf("Master: PID %d requesting write of address %d in page %d at time %d:%d\n", process, addr, page, *clockSec, *clockNano);
                        fprintf(logfile, "Master: PID %d requesting write of address %d in page %d at time %d:%d\n", process, addr, page, *clockSec, *clockNano);
			for(int i = 0; i<18; i++)
			{
				if(processTable[i].pid == process)	//get correct process
				{
					if(processTable[i].page[page].occupied == 1)	//if page occupied
					{
						timePassed = 100;
				                incrementClock(clockSec, clockNano, timePassed);
						delayTime += 100;
                                                if(delayTime >= 1000000000)
                                                {
                                                        delayTime -= 1000000000;
                                                        delaySec++;
                                                }
//						printf("Master: Address %d in frame %d, writing data to PID %d at time %d:%d\n", addr, processTable[i].page[page].frame, process, *clockSec, *clockNano);
 						fprintf(logfile, "Master: Address %d in frame %d, writing data to PID %d at time %d:%d\n", addr, processTable[i].page[page].frame, process, *clockSec, *clockNano);
						//acknowledge in worker
						buf0.mtype = process;
                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                        perror("msgsnd to worker failed\n");
                                                	exit(1);
                                                }
					}
					else	//page fault 	//not loaded put into table
					{
						bool full = true;
//						printf("Master: Address %d is not in a frame, pagefault\n", addr);	
                                                fprintf(logfile, "Master: Address %d is not in a frame, pagefault\n", addr);
						pagefault++;
						//put in open frame
						for(int j =0; j<256; j++)
						{
							if(frameTable[j].occupied == 0)	//if frame available use it
							{
								full= false;
								frameTable[j].occupied = 1;	//make it occupied
								//save the frame and that it is now occupied
								processTable[i].page[page].occupied = 1;
                                                                processTable[i].page[page].frame = j; 
//								printf("Master: Open frame found, Loading page %d in frame %d\n", page, processTable[i].page[page].frame);
                                                                fprintf(logfile, "Master: Open frame found, Loading page %d in frame %d\n", page, processTable[i].page[page].frame);
								headq.push(j);
								if(headq.size() <= 1)
								{
                                                                	frameTable[headq.front()].head = '*';
								}
								//acknowledges in worker
								buf0.mtype = process;
		                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
        		                                                perror("msgsnd to worker failed\n");
                		                                        exit(1);
                        		                        }
								break;
							}
						}
						if(full ==true)	//no open frame available, swapping
						{
//							printf("frame table is full, %d is head\n", headq.front());		
                                                        fprintf(logfile, "Frame table is full, %d is head\n", headq.front());
//							printf("Clearing frame %d and swapping PID %d page %d\n", headq.front(), process, page);
                                                        fprintf(logfile, "Clearing frame %d and swapping PID %d page %d\n", headq.front(), process, page);
        						for(int j=0; j < 256; j++)
                                                        {
                                                                if(headq.front() == j )     //if frame is the head and front of the queue
                                                                {
                                                                        if(frameTable[j].dirty == 1)    //if its dirty more time and reset for new frame
                                                                        {
//                                                                              printf("Master: Dirty bit of frame %d found, adding additional time to the clock\n", j);
                                                                                fprintf(logfile, "Master: Dirty bit of frame %d found, adding additional time to the clock\n", j);
                                                                                incrementClock(clockSec, clockNano, 300000000);
										frameTable[j].dirty = 0;    //reset dirty bit

                                                                        }

									frameTable[headq.front()].head = ' ';
                                                                	headq.push(headq.front());        
									headq.pop();
									frameTable[headq.front()].head = '*';
                                                                        processTable[i].page[page].frame = j;
                                                                        buf0.mtype = process;
                                                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                                perror("msgsnd to worker failed\n");
                                                                                exit(1);
                                                                        }
									break;
                                                                }
                                                        }
						}
						incrementClock(clockSec, clockNano, 140000000);
						delayTime += 140000000;	
						if(delayTime >= 1000000000)
                                                {
                                                        delayTime -= 1000000000;
                                                        delaySec++;
                                                }
					}
					if(frameTable[processTable[i].page[page].frame].dirty == 0)
					{
						frameTable[processTable[i].page[page].frame].dirty = 1;
//						printf("Master: Dirty bit of frame %d set for PID %d\n", processTable[i].page[page].frame, process);
                                                fprintf(logfile, "Master: Dirty bit of frame %d set for PID %d\n", processTable[i].page[page].frame, process);

					}
				}
			}
		}
		
		else if(decision  == 3)
		{  
//			printf("PID %d terminated. Released: ", process);
                        fprintf(logfile, "PID %d terminated. Released: ", process);
                        buf0.mtype = process;
                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                perror("msgsnd to worker failed\n");
                                exit(1);
                        }
                	//printf("term before loop\n");
		        for(int i = 0; i < 18; i++)
                        {
                                if(processTable[i].pid == process)	//find process
                                {
					for(int j = 0; j <32; j++)	//look through all pages
					{
						if(processTable[i].page[j].occupied == 1)	//if page is there, free it
						{
//							printf("Frame: %d ", processTable[i].page[j].frame);
                                                        fprintf(logfile, "Frame: %d ", processTable[i].page[j].frame);
							for(int q =0; q< headq.size(); q++)
							{
								if(headq.front()== processTable[i].page[j].frame)
								{
									headq.pop();
								}
								else
								{
									headq.push(headq.front());
									headq.pop();
								}
							}
							processTable[i].page[j].occupied = 0;
							frameTable[processTable[i].page[j].frame].occupied = 0;
                                                        frameTable[processTable[i].page[j].frame].dirty = 0;
							frameTable[processTable[i].page[j].frame].head = ' ';
							processTable[i].page[j].frame = -1;	
						}
					}
                                        break;
                                }
                        }
			fprintf(logfile, "\n");
        		removeProcess(processTable, process);
                        printTable(processTable, *clockSec, *clockNano, logfile);
			printFrame(frameTable, logfile, headq);
			terminateChild++;
                        wait(NULL);
		}

		}
		incrementClock(clockSec, clockNano, 100000000);
	}	

	system("rm msgq.txt");

	printf("oss is now ending\n");
	showq(headq, logfile);
        
	printf("STATISTICS\nProcesses ran: %d, Terminated: %d, Start Time: %d, End Time: %d, Current Time: %d\n", proc, terminateChild, stime,ntime, ctime);
	printf("Page Faults: %d\nMemory Accesses: %d\nDelay Time: %d:%d\n", pagefault, readWrite, delaySec, delayTime);
	printf("Total Msgs: %d\n", totalMsgs);
	fprintf(logfile, "STATISTICS\nProcesses ran: %d, Terminated: %d, Start Time: %d, End Time: %d, Current Time: %d\n", proc, terminateChild, stime,ntime, ctime);
        fprintf(logfile, "Page Faults: %d\nMemory Accesses: %d\nDelay Time: %d:%d\n", pagefault, readWrite, delaySec, delayTime);
        fprintf(logfile, "Total Msgs sent: %d\n", totalMsgs);



	double accessPerSec = 0;
        accessPerSec = (double) readWrite / *clockSec;
        printf("Number of memory accesses per second: %f\n", accessPerSec);
        fprintf(logfile, "Number of memory accesses per second: %f\n", accessPerSec);

        double faultsPerAccess = 0;
	faultsPerAccess = (double) pagefault / readWrite;
	printf("Number of page faults per memory access: %f\n", faultsPerAccess);
	fprintf(logfile, "Number of page faults per memory access: %f\n", faultsPerAccess);

        double accessSpeedSec = 0, accessSpeedNS;
	accessSpeedSec = (double) delaySec / readWrite;
	accessSpeedNS = (double) delayTime / readWrite;
	printf("Average memory access speed: %f : %f\n", accessSpeedSec, accessSpeedNS);
	fprintf(logfile, "Average memory access speed: %f : %f\n", accessSpeedSec, accessSpeedNS);


	shmdt( clockSec );    // Detach from the shared memory segment
        shmctl( idSec, IPC_RMID, NULL ); // Free shared memory segment shm_id

	shmdt( clockNano );    // Detach from the shared memory segment
        shmctl( idNano, IPC_RMID, NULL ); // Free shared memory segment shm_id
     
	// get rid of message queue
        if (msgctl(msqid, IPC_RMID, NULL) == -1) {
                perror("msgctl to get rid of queue in parent failed");
                exit(1);
        }

	return EXIT_SUCCESS;
}
